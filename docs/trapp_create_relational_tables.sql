-- SET ANSI_NULLS ON;
-- SET QUOTED_IDENTIFIER ON;
-- SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
/* Create table Currency */

CREATE TABLE Currency(
    ID INT NOT NULL,
    NAME NVARCHAR(100) NOT NULL,
    COUNTRY_OF_ORIGIN NVARCHAR(3) NOT NULL,
    EXCHANGE_RATE_DOLLARS FLOAT(23) NOT NULL,
    PRIMARY KEY (ID)
);

/* Create table Place */

CREATE TABLE Place(
    ID INT NOT NULL,
    NAME NVARCHAR(100) NOT NULL,
    ADDRESS NVARCHAR(100) NOT NULL,
    LONGITUDE FLOAT(23) NOT NULL,
    LAITUDE FLOAT(23) NOT NULL,
    City_ID INT NOT NULL,
    PRIMARY KEY (ID)
);
/* TODO: One to many con Activity */


/* Create table City */

CREATE TABLE City(
    ID INT NOT NULL,
    NAME NVARCHAR(100) NOT NULL,
    LONGITUDE FLOAT(23) NOT NULL,
    LAITUDE FLOAT(23) NOT NULL,
    PRIMARY KEY (ID)
);

/* TODO: One to many hacia place */

/* Create table Pack_Item */

CREATE TABLE Pack_Item(
    ID INT NOT NULL,
    NAME NVARCHAR(100) NOT NULL,
    PRIMARY KEY (ID)
);

/* Create table Budget */

CREATE TABLE Budget(
    ID INT NOT NULL,
    TOTAL_BUDGET FLOAT(23) NOT NULL,
    SPENT_BUDGET FLOAT(23) NOT NULL,
    Travel_Plan_ID INT NOT NULL,
    PRIMARY KEY (ID)
);

/* Create table Activity */

CREATE TABLE Activity(
    ID INT NOT NULL,
    NAME NVARCHAR(100) NOT NULL,
    PRICE FLOAT(23) NOT NULL,
    START_DATE DATETIME NOT NULL,
    END_DATE DATETIME NOT NULL,    
    Place_ID INT NOT NULL,
    PRIMARY KEY (ID)
);

/* Create table Trip */

CREATE TABLE Trip(
    ID INT NOT NULL,
    NAME NVARCHAR(100) NOT NULL,
    PRICE FLOAT(23) NOT NULL,
    START_DATE DATETIME NOT NULL,
    END_DATE DATETIME NOT NULL,   
    Travel_Plan_ID INT NOT NULL,
    Origin_ID INT NOT NULL,
    Destination_ID INT NOT NULL,    
    PRIMARY KEY (ID)
);

/* Create table Review */

CREATE TABLE Review(
    ID INT NOT NULL,
    CALIFICATION INT NOT NULL,
    RATIONALE NVARCHAR(1000) NOT NULL,
    Trip_ID INT NOT NULL,
    PRIMARY KEY (ID)
);

/* Create table Media */

CREATE TABLE Media(
    ID INT NOT NULL,
    NAME NVARCHAR(100) NOT NULL,
    MEDIA_DATE DATETIME NOT NULL,
    MEDIA_TYPE NVARCHAR(20) NOT NULL,
    MEDIA_PATH NVARCHAR(500) NOT NULL,
    Travel_Plan_ID INT NOT NULL,
    Travel_User_ID INT NOT NULL,
    PRIMARY KEY (ID)
);

/* Create table Travel_Plan */

CREATE TABLE Travel_Plan(
    ID INT NOT NULL,
    NAME NVARCHAR(100) NOT NULL,
    PRICE FLOAT(23) NOT NULL,
    Travel_User_Group_ID INT NOT NULL,
    Budget_ID INT NOT NULL,
    PRIMARY KEY (ID),
);

/* Create table Travel_User_Group */

CREATE TABLE Travel_User_Group(
    ID INT NOT NULL,
    NUMBER_MEMBERS INT NOT NULL,
    TOTAL_BUDGET FLOAT(23) NOT NULL,
    PRIMARY KEY (ID)
);

/* Create table Travel_User */

CREATE TABLE Travel_User(
    ID INT NOT NULL,
    FIRST_NAME NVARCHAR(100) NOT NULL,
    LAST_NAME NVARCHAR(100) NOT NULL,
    GENDER NVARCHAR(100) NOT NULL,
    EMAIL NVARCHAR(100) NOT NULL,
    PHONE NVARCHAR(100) NOT NULL,
    BIRTH_DATE DATE NOT NULL,
    PRIMARY KEY (ID)
);

/* RELACIONES */

/* One to Many | City - Place */

ALTER TABLE Place
ADD CONSTRAINT FK_City_Place
FOREIGN KEY (City_ID) REFERENCES City(ID) ON DELETE CASCADE;

/* One to Many | Place - Activity */

ALTER TABLE Activity
ADD CONSTRAINT FK_Place_Activity
FOREIGN KEY (Place_ID) REFERENCES Place(ID) ON DELETE CASCADE;

/* One to Many | Trip - Review */

ALTER TABLE Review
ADD CONSTRAINT FK_Trip_Review
FOREIGN KEY (Trip_ID) REFERENCES Trip(ID) ON DELETE CASCADE;

/* One to Many | Travel_Plan - Trip */

ALTER TABLE Trip
ADD CONSTRAINT FK_Travel_Plan_Trip
FOREIGN KEY (Travel_Plan_ID) REFERENCES Travel_Plan(ID) ON DELETE CASCADE;

/* One to Many | Travel_User_Group - Travel_Plan */

ALTER TABLE Travel_Plan
ADD CONSTRAINT FK_Travel_User_Group_Travel_Plan
FOREIGN KEY (Travel_User_Group_ID) REFERENCES Travel_User_Group(ID) ON DELETE CASCADE;

/* One to Many | Travel_Plan - Media */

ALTER TABLE Media
ADD CONSTRAINT FK_Travel_Plan_Media
FOREIGN KEY (Travel_Plan_ID) REFERENCES Travel_Plan(ID) ON DELETE CASCADE;

/* One to Many | Travel_User - Media */

ALTER TABLE Media
ADD CONSTRAINT FK_Travel_User_Media
FOREIGN KEY (Travel_User_ID) REFERENCES Travel_User(ID) ON DELETE CASCADE;

/* Many to Many | Travel_User - Travel_User_Group */

CREATE TABLE Travel_User_Travel_User_Group(
    Travel_User_ID INT NOT NULL,
    Travel_User_Group_ID INT NOT NULL,
    CONSTRAINT Many_To_Many_Travel_User FOREIGN KEY (Travel_User_ID) REFERENCES Travel_User(ID),
    CONSTRAINT Many_To_Many_Travel_User_Group FOREIGN KEY (Travel_User_Group_ID) REFERENCES Travel_User_Group(ID)
);

/* Many to Many | Trip - Pack_Item */

CREATE TABLE Trip_Pack_Item(
    Trip_ID INT NOT NULL,
    Pack_Item_ID INT NOT NULL,
    CONSTRAINT Many_To_Many_Trip FOREIGN KEY (Trip_ID) REFERENCES Trip(ID),
    CONSTRAINT Many_To_Many_Pack_Item FOREIGN KEY (Pack_Item_ID) REFERENCES Pack_Item(ID)
);

/* One to One | Travel_Plan - Budget */

ALTER TABLE Travel_Plan
ADD CONSTRAINT FK_Travel_Plan_Budget 
FOREIGN KEY (Budget_ID) REFERENCES Budget(ID);

ALTER TABLE Budget
ADD CONSTRAINT FK_Budget_Travel_Plan 
FOREIGN KEY (Travel_Plan_ID) REFERENCES Travel_Plan(ID);

/* Foreign Key | Trip: Origen & Destino*/

ALTER TABLE Trip
ADD CONSTRAINT FK_Trip_Origin
FOREIGN KEY (Origin_ID) REFERENCES City(ID);

ALTER TABLE Trip
ADD CONSTRAINT FK_Trip_Destination
FOREIGN KEY (Destination_ID) REFERENCES City(ID);