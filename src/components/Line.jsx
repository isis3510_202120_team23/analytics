import React from 'react'
import { ComposedChart, ReferenceLine, Brush, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';


// var stringToColour = function (str) {
//   var hash = 0;
//   for (var i = 0; i < str.length; i++) {
//     hash = str.charCodeAt(i) + ((hash << 5) - hash);
//   }
//   var colour = '#';
//   for (var i = 0; i < 3; i++) {
//     var value = (hash >> (i * 8)) & 0xFF;
//     colour += ('00' + value.toString(16)).substr(-2);
//   }
//   return colour;
// }

export default function LineChartAll({data,cat,val}) {
  return (
    <ResponsiveContainer width="100%" height="100%">
      <ComposedChart
        width={500}
        height={400}
        data={data}
        margin={{
          top: 20,
          right: 20,
          bottom: 20,
          left: 20,
        }}
      >
        <CartesianGrid strokeDasharray="5 5" />
        <XAxis dataKey={cat} scale='band' padding={{ left: 30, right: 30 }} />
        <YAxis />
        <Tooltip  />
        <Legend/>
        <ReferenceLine y={0} stroke="#000" />
        <Brush dataKey="nombre" height={30} stroke="#8884d8" />
        
        <Line type="monotone" dataKey={val} fill="#356456" />
      </ComposedChart>
    </ResponsiveContainer>
  );
}