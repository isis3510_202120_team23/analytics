import React from 'react'
import { ComposedChart, Bar, ReferenceLine, Brush, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Area, Line } from 'recharts';

export default function LineChartAll({ data, cat, val, val2, val3, info }) {
    return (
        <ResponsiveContainer width="100%" height="100%">
            <ComposedChart
                width={500}
                height={400}
                data={data}
                margin={{
                    top: 20,
                    right: 20,
                    bottom: 20,
                    left: 20,
                }}
            >
                <CartesianGrid strokeDasharray="5 5" />
                <XAxis dataKey={cat} scale='band' padding={{ left: 30, right: 30 }} />
                <YAxis />
                <Tooltip />
                <Legend />
                <ReferenceLine y={0} stroke="#000" />
                <Brush dataKey="nombre" height={30} stroke="#8884d8" />

                <Bar type="monotone" dataKey={val} fill="#4b3268" />
                <Line type="monotone" dataKey={val2} fill="#ff0000" activeDot={{ r: 8 }} />
                <defs>
                    <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                        <stop offset="5%" stopColor="#fa03fc" stopOpacity={0.8} />
                        <stop offset="95%" stopColor="#000000" stopOpacity={0.4} />
                    </linearGradient>
                </defs>
                <Area dataKey={val3} fill="url(#colorUv)" opacity={0.8} />
            </ComposedChart>
        </ResponsiveContainer>
    );
}