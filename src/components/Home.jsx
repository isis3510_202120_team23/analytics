import { React, useEffect, useState } from "react";
import Line from "./Line";
import Bar from "./Bar";

import CustomComposedChartQ4 from "./CustomComposedChartQ4";

import { initializeApp } from "firebase/app";
import {
	getFirestore,
	doc,
	getDoc,
	collection,
	query,
	getDocs, // setDoc, updateDoc
} from "firebase/firestore";

const app = initializeApp({
	apiKey: "AIzaSyCwvJBHqC8-1LOxeLgNaBb0_HRi8vRpSY8",
	authDomain: "trapp-f9ea1.firebaseapp.com",
	projectId: "trapp-f9ea1",
	storageBucket: "trapp-f9ea1.appspot.com",
	messagingSenderId: "13840337353",
	appId: "1:13840337353:web:1c17836ecf01e9339cc018",
	measurementId: "G-PDFVH93H14",
});

const db = getFirestore(app);

//Anderson: best seller plans and cities

export default function Home() {
	const [dataBestPlans, setBestPlans] = useState([]);
	const [bestTrips, setBestTrips] = useState([]);
	const [dataRates, setRates] = useState([]);
	const [dataUserTime, setUserTime] = useState([]);
	const [dataAveragePlanning, setAveragePlanning] = useState([]);
	const [dataAverageAppUsageTime, setAverageAppUsageTime] = useState([]);
	const [dataUserGroup, setTipeOfTrip] = useState([]);
	const [dataTimeLoading, setTimeLoading] = useState([]);
	const [dataCitiesPerPlans, setCitiesPerPlans] = useState([]);
	const [dataPlans, setPlans] = useState([]);
	const [dataReviewsPerCities, setReviewsPerCities] = useState([]);
	useEffect(() => {
		getCitiesRatings();
		getTripsXd();

		getAllDocuments("userGroup").then((data) => {
			let individual = 0;
			let group = 0;
			let res = [];

			data.forEach(function (element) {
				if (element.numberMembers > 1) {
					group += 1;
				} else {
					individual += 1;
				}
			});

			res.push({ typeOfTrip: "Group", total: group });
			res.push({ typeOfTrip: "Individual", total: individual });
			setTipeOfTrip(res);
		});
		/*getAllDocuments("plans").then((data) => {
			let dtemp = [...data];
			data.forEach((e) => {
				e.value = (e.price * e.amountOfSales) / 100;
				e.amountOfSales *= 5000;
				e.info = `${e.name} -> ${e.city.path.substring(7)}`;
			});
			let dt = dtemp.sort((a, b) => b.value - a.value);
			setBestPlans(dt);
		});*/
		//Analytics Question: What is the least visited screen in the app?
		getAllDocuments("userTime").then((data) => {
			leastVisitedScreen(data);
			averagePlanning(data);
			averageAppUsage(data);
		});
		getAllDocuments("loadingTime").then((data) => {
			loadingTime(data);
			bestSellingTrips(data);
		})
		getAllDocuments("plans").then((data) => {
			getPlansPerMonth(data);
		})
		getAllDocuments("plans_trips").then((data) => {
			bestSellingTrips(data);
		})
		getAllDocuments("trips").then((data) => {
			let a = [];
			let res = [];
			data.forEach(function (element) {
				let cal = 0;
				element.reviews.forEach((r) => (cal = cal + r.califiction));
				a.push({ trip: element.name, calification: cal * 0.5 });
			});
			let max = 0;
			let min = 5;
			let maxEl;
			let minEl;
			let suma = 0;
			let i = 1;
			a.forEach(function (element) {
				if (element.calification > max) {
					maxEl = element;
					max = element.calification;
				}
				if (element.calification < min) {
					minEl = element;
					min = element.calification;
				}
				suma += element.calification;
				i++;
			});
			res.push({
				trip: "Max ->" + maxEl.trip,
				calification: maxEl.calification,
			});
			res.push({ trip: "Average", calification: suma / i });
			res.push({
				trip: "Min ->" + minEl.trip,
				calification: minEl.calification,
			});

			setRates(res);
		});
	}, []);

	async function getAllDocuments(c) {
		const toRet = [];
		const q = query(collection(db, c));

		const querySnapshot = await getDocs(q);
		querySnapshot.forEach((doc) => {
			// doc.data() is never undefined for query doc snapshots
			toRet.push({
				...doc.data(),
				id: doc.id,
			});
		});
		return toRet;
	}

	async function getOneDocument(col, id) {
		const ref = doc(db, col, id);
		const docSnap = await getDoc(ref);
		if (docSnap.exists()) {
			return docSnap.data();
		} else {
			// console.log("No se encontró el documento de la colección " + col + " con id " + id);
			return null;
		}
	}

	function bestSellingTrips(pls) {
		let acc = new Map();
		let ret = [];
		pls.forEach(p =>
			p?.trips?.forEach(t =>
				acc.has(t.name) ?

					acc.set(t.name, { name: t.name, val: acc.get(t.name).val + t.price })
					:
					acc.set(t.name, { name: t.name, val: t.price })
			)
		)
		acc.forEach(v => ret.push(v));
		setBestTrips(ret);
	}

	//Analytics Question: What is the least visited screen in the app?

	function leastVisitedScreen(data) {
		let timeLogin = 0;
		let timePackItem = 0;
		let timePlan = 0;
		let timeSignUp = 0;
		let timeStart = 0;
		let res = [];

		data.forEach(function (element) {
			if (element.activity === "SignIn") {
				timeLogin += element.time;
			} else if (element.activity === "PackItem") {
				timePackItem += element.time;
			} else if (element.activity === "Plan") {
				timePlan += element.time;
			} else if (element.activity === "SignUp") {
				timeSignUp += element.time;
			} else if (element.activity === "Start") {
				timeStart += element.time;
			}
		});
		res.push({ label: "SignIn", totalVisits: timeLogin });
		res.push({ label: "PackItem", totalVisits: timePackItem });
		res.push({ label: "Plan", totalVisits: timePlan });
		res.push({ label: "SignUp", totalVisits: timeSignUp });
		res.push({ label: "Start", totalVisits: timeStart });
		setUserTime(res);
	}

	function averagePlanning(data) {
		let timePlan = 0;
		let planNumberVisits = 0;

		let res = [];

		data.forEach(function (element) {
			if (element.activity === "Plan") {
				timePlan += element.time;
				planNumberVisits++;
			}
		});

		const average = timePlan / planNumberVisits;

		res.push({ label: "Average Planning Time", averagePlanningTime: average });
		setAveragePlanning(res);
	}

	// What is the average app usage time?
	function averageAppUsage(data) {
		let timeUsage = 0;
		let Users = 0;

		let res = [];

		data.forEach(function (element) {
			timeUsage += element.time;
			Users++;
		});

		const average = timeUsage / Users;

		res.push({ label: "Average App Usage Time", averageAppUsageTime: average });
		setAverageAppUsageTime(res);
	}

	function loadingTime(data) {
		let Plan = 0;
		let countPlan = 0;
		let Start = 0;
		let countStart = 0;
		let SignIn = 0;
		let countSignIn = 0;
		let SignUp = 0;
		let countSignUp = 0;
		let Budget = 0;
		let countBudget = 0;
		let Packing = 0;
		let countPacking = 0;
		let res = [];
		data.forEach((item) => {
			if (item.activity === "Plan") {
				Plan += item.time
				countPlan += 1
			}
			else if (item.activity === "Start") {
				Start += item.time
				countStart += 1
			}
			else if (item.activity === "SignIn") {
				SignIn += item.time
				countSignIn += 1
			}
			else if (item.activity === "SignUp") {
				SignUp += item.time
				countSignUp += 1
			}
			else if (item.activity === "Budget") {
				Budget += item.time
				countBudget += 1
			}
			else if (item.activity === "Packing") {
				Packing += item.time
				countPacking += 1
			}
		})

		res.push({ Activity: "Plan", time: Plan / (countPlan * 1000) });
		res.push({ Activity: "Start", time: Start / (countStart * 1000) });
		res.push({ Activity: "SignIn", time: SignIn / (countSignIn * 1000) });
		res.push({ Activity: "SignUp", time: SignUp / (countSignUp * 1000) });
		res.push({ Activity: "Budget", time: Budget / (countBudget * 1000) });
		res.push({ Activity: "Packing", time: Packing / (countPacking * 1000) });
		setTimeLoading(res)
	}
	//BQ --> How many trips have been planned per city?
	async function getTripsPerCity() {
		return new Promise((resolve, reject) => {
			const map = new Map();
			getAllDocuments("plansMario").then((plans) => {
				plans.forEach(function (plan) {
					if (plan.trips != null) {
						plan.trips.forEach(function (trip) {
							getOneDocument("trips", trip.id).then((trip) => {
								if (trip == null) {

								}
								else {
									getOneDocument("cities", trip.destination.id).then((city) => {
										if (map.get(city.name) === undefined) {
											map.set(city.name, 1)
										}
										else {
											const currentValue = map.get(city.name);
											map.set(city.name, currentValue + 1);
										}
									});
								}
							}).catch();
						});
					}
				});
				resolve(map);
			}
			);
		});
	}

	//Which are the cities with the worst ratings?
	function getCitiesRatings() {
		const mapTotal = new Map();
		const mapCount = new Map();

		getAllDocuments("cities").then((cities) => {
			cities.forEach(function (city) {
				const cityPlaces = city.places;
				cityPlaces.forEach(function (place) {
					getOneDocument("places", place.id).then((placeFound) => {
						const reviews = placeFound.reviews;
						if (reviews != null && Array.isArray(reviews)) {
							if (reviews)
								reviews.forEach(function (review) {
									if (mapTotal.get(city.name) === undefined) {
										mapTotal.set(city.name, review.calification);
										mapCount.set(city.name, 1);
									}
									else {
										const currentValueTotal = mapTotal.get(city.name);
										const currentValueCount = mapCount.get(city.name);
										mapTotal.set(city.name, currentValueTotal + review.calification);
										mapCount.set(city.name, currentValueCount + 1);
									}
								})
						}
					})
				})
			})
		})

		setTimeout(() => {

			const mapAvg = new Map();

			for (const [key, value] of mapTotal.entries()) {
				const total = mapTotal.get(key);
				const count = mapCount.get(key);
				const avg = total / count;

				mapAvg.set(key, avg);
			}

			const array = Array.from(mapAvg, ([city, average]) => ({ city, average }));
			setReviewsPerCities(array);
		}, 9000)
	}


	async function getTripsXd() {
		let map = await getTripsPerCity();
		setTimeout(() => {

			// console.log(map);
			const array = Array.from(map, ([city, count]) => ({ city, count }));
			setCitiesPerPlans(array);
		}, 3000)
	}
	async function getPlansPerMonth(plans) {
		let res = []
		let months = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0 }
		plans.forEach((item) => {
			months[item.month] += 1
		})
		setPlans(months)
		res.push({ Month: "January", plans: months[1] });
		res.push({ Month: "February", plans: months[2] });
		res.push({ Month: "March", plans: months[3] });
		res.push({ Month: "April", plans: months[4] });
		res.push({ Month: "May", plans: months[5] });
		res.push({ Month: "June", plans: months[6] });
		res.push({ Month: "July", plans: months[7] });
		res.push({ Month: "August", plans: months[8] });
		res.push({ Month: "September", plans: months[9] });
		res.push({ Month: "October", plans: months[10] });
		res.push({ Month: "November", plans: months[11] });
		res.push({ Month: "December", plans: months[12] });
		setPlans(res)
	}
	return (
		<div>
			<div style={{ width: '100vw', display: "flex", alignContent: "center", paddingLeft: '12.5vw', marginTop: '50px' }}>
				<div style={{ display: "flex", flexDirection: "column" }}>
					<h1>
						Sprint 4
					</h1>
				</div>
			</div>
				<div style={{ width: "800px", height: "800px" }}>
				<h2> Which are the best selling plans? </h2>
						<Bar data={bestTrips} cat={"name"} val={"val"} />

					<h3>What is the satisfaction rate of planned trips?</h3>
					<Line data={dataRates} cat={"trip"} val={"calification"} />

					<br />

					<h3>What is the least visited screen in the app?</h3>
					<Line data={dataUserTime} cat={"label"} val={"totalVisits"} />

					<br />

					<h3>What is the average time that users spend choosing a trip?</h3>
					<Bar
						data={dataAveragePlanning}
						cat={"label"}
						val={"averagePlanningTime"}
					/>

					<br />

					<h3>What is the average app usage time?</h3>
					<Bar
						data={dataAverageAppUsageTime}
						cat={"label"}
						val={"averageAppUsageTime"}
					/>

					<br />

					<h2>BQ Sprint 3</h2>

					<h3>In summary which screens spend more time loading?</h3>
					<Bar data={dataTimeLoading} cat={"Activity"} val={"time"} />


					<h3>How many trips have been planned per city?</h3>
					<Bar
						data={dataCitiesPerPlans}
						cat={"city"}
						val={"count"}
					/>


					<h3>Which are the months when the most plans are created?</h3>
					<Bar
						data={dataPlans}
						cat={"Month"}
						val={"plans"}
					/>


					
						<h3>
							What type of trip is preferred by users? (individual or group trip)
						</h3>
						<Bar data={dataUserGroup} cat={"typeOfTrip"} val={"total"} />


						<h4>Activity vs  Average time loading (miliseconds)</h4>
						<Bar data={dataTimeLoading} cat={"Activity"} val={"time"} />


						<h3>How many trips have been planned per city?</h3>
						<Bar
							data={dataCitiesPerPlans}
							cat={"city"}
							val={"count"}
						/>


						<h3>Which are the cities with the worst average ratings?</h3>
						<Bar
							data={dataReviewsPerCities}
							cat={"city"}
							val={"average"}
						/>

					</div>
				</div>
				);
}
