import React from "react";
import {
	ComposedChart,
	Area,
	ReferenceLine,
	Brush,
	XAxis,
	YAxis,
	CartesianGrid,
	Tooltip,
	Legend,
	ResponsiveContainer,
} from "recharts";

export default function LineChartAll({ data, cat, val }) {
	return (
		<ResponsiveContainer width="100%" height="100%">
			<ComposedChart
				width={500}
				height={400}
				data={data}
				margin={{
					top: 20,
					right: 20,
					bottom: 20,
					left: 20,
				}}
			>
				<CartesianGrid strokeDasharray="5 5" />
				<XAxis dataKey={cat} scale="band" padding={{ left: 30, right: 30 }} />
				<YAxis />
				<Tooltip />
				<Legend />
				<ReferenceLine y={0} stroke="#000" />
				<Brush dataKey="nombre" height={30} stroke="#8884d8" />

				<Area type="monotone" dataKey={val} fill="#356456" />
			</ComposedChart>
		</ResponsiveContainer>
	);
}
