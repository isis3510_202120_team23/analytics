var Connection = require("tedious").Connection;
var Request = require("tedious").Request;
var TYPES = require("tedious").TYPES;
const debug = require("debug")("firestore-snippets-node");

/* SQLSERVER */

// Create connection to database
var config = {
	server: "moviles202120.database.windows.net",
	authentication: {
		options: {
			userName: "movilesadminuser",
			password: "CwfCBfXxrNRJwX9",
		},
		type: "default",
	},
	options: {
		database: "moviles",
	},
};
var connection = new Connection(config);
// Attempt to connect and execute queries if connection goes through
connection.on("connect", (err) => {
	if (err) {
		console.error(err.message);
	} else {
		queryDatabase();
	}
});

connection.connect();

function sqlInsert(name, location, callback) {
	console.log("Inserting '" + name + "' into Table...");

	let request = new Request(
		"INSERT INTO TestSchema.Employees (Name, Location) OUTPUT INSERTED.Id VALUES (@Name, @Location);",
		function (err, rowCount, rows) {
			if (err) {
				callback(err);
			} else {
				console.log(rowCount + " row(s) inserted");
				callback(null, "Nikita", "United States");
			}
		}
	);
	request.addParameter("Name", TYPES.NVarChar, name);
	request.addParameter("Location", TYPES.NVarChar, location);
}

function queryDatabase() {
	console.log("Reading rows from the Table...");

	// Read all rows from table
	let request = new Request(`Select * From review`, (err, rowCount) => {
		if (err) {
			console.error(err.message);
		} else {
			console.log(`${rowCount} row(s) returned`);
		}
	});

	request.on("row", (columns) => {
		columns.forEach((column) => {
			console.log("%s\t%s", column.metadata.colName, column.value);
		});
	});
	connection.execSql(request);
}
